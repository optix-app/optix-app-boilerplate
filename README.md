> This section exists as reminder of what is desirable at the README file. You MUST REMOVE this section once you write the contents of this document.

Make sure to add your app description here, including:

- How to install the app
- How to set up the local development
- What canvases should be set
- Add anything important for future developers

---

This project uses Optix Boilerplate as starting point, you can learn how to use the boilerplate reading [Boilerplate docs](BOILERPLATE.md)

---

# App name

Short description of the app.

## Running this app locally

This app uses:
- Laravel 9
- PHP 8.1
- Laravel Sail
- Laravel Horizon

## App settings file

This is the suggested app settings file for this project:

```json
{
    ... Place your settings file here
}
```
